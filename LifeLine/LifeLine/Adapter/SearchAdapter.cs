﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using LifeLine.DataModels;
using Android.Media;
using FFImageLoading;

namespace LifeLine.Adapter
{
    class SearchAdapter : RecyclerView.Adapter
    {
        public event EventHandler<SearchAdapterClickEventArgs> ItemClick;
        public event EventHandler<SearchAdapterClickEventArgs> ItemLongClick;
        List<SearchProductDisplay> items;

        public SearchAdapter(List<SearchProductDisplay> data)
        {
            items = data;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = null;

            itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.SearchResult, parent, false);


            var vh = new SearchAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = items[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as SearchAdapterViewHolder;
            holder.searchProduct.Text = item.SearchProductName;
            holder.searchProductPrice.Text = item.SearchPrice;
            Getimage(item.SearchImageUrl, holder.searchProductImage);
        }
        void Getimage(string url, ImageView imageView)
        {
            ImageService.Instance.LoadUrl(url)
             .Retry(3, 200)
             .DownSample(400, 400)
             .Into(imageView);
        }

        public override int ItemCount => items.Count;

        void OnClick(SearchAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(SearchAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class SearchAdapterViewHolder : RecyclerView.ViewHolder
    {
        //public TextView TextView { get; set; }
        public TextView searchProduct;
        public TextView searchProductPrice;
        public ImageView searchProductImage;
        public ImageView AddtoCart;


        public SearchAdapterViewHolder(View itemView, Action<SearchAdapterClickEventArgs> clickListener,
                            Action<SearchAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            //TextView = v;
            itemView.Click += (sender, e) => clickListener(new SearchAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new SearchAdapterClickEventArgs { View = itemView, Position = AdapterPosition });

            searchProduct = (TextView)itemView.FindViewById(Resource.Id.mainPageProductName);
            searchProductPrice = (TextView)itemView.FindViewById(Resource.Id.mainPageProductPrice);
            searchProductImage = (ImageView)itemView.FindViewById(Resource.Id.mainPageProductImage);
            AddtoCart = (ImageView)itemView.FindViewById(Resource.Id.mainPageProductCart);

        }
    }

    public class SearchAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}