﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = false)]
    public class ChangePassword : AppCompatActivity
    {
        MyAccount main = new MyAccount();
        //decalration
        EditText changepassword, confirmchangepassword;
        ImageView confirmchange, declinechange;
        protected override void OnCreate(Bundle savedInstanceState)
        {

            SetContentView(Resource.Layout.ChangePassword);

            //initializing values
            changepassword = (EditText)FindViewById(Resource.Id.changepassEditText);
            confirmchangepassword = (EditText)FindViewById(Resource.Id.changeconfirmpassEditText);
            confirmchange = (ImageView)FindViewById(Resource.Id.confirmChangepass);
            declinechange = (ImageView)FindViewById(Resource.Id.declinepass);

            //click events 
            confirmchange.Click += Confirmchange_Click;
            declinechange.Click += Declinechange_Click;
        }

        private void Declinechange_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(MyAccount));
            StartActivity(intent);
        }

        private void Confirmchange_Click(object sender, EventArgs e)
        {
            User user = new User();            
           

            if (changepassword.Text == confirmchangepassword.Text){
               

                
                Toast.MakeText(this, "Password Change Successful ", ToastLength.Short).Show();
                Intent intent = new Intent(this, typeof(MyAccount));
                StartActivity(intent);

            }
            }
    }
}