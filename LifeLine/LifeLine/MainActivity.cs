﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Views;
using Android.Content;

using LifeLine.Resources.Activites;
using Android.Content.Res;
using Android.Webkit;
using LifeLine.EventListeners;
using Android.Support.V7.Widget;
using Android.Support.V7.RecyclerView.Extensions;
using LifeLine.Adapter;
using System.Collections.Generic;
using LifeLine.Helpers;
using LifeLine.DataModels;
using System.Linq;

namespace LifeLine
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = false)]
    public class MainActivity : AppCompatActivity
    {
        //adapter declaration 
        RecyclerView productDisplay;
        mainProductDisplayAdapter mainProductDisplayAdapter;
        public List<MainProductDisplay> ListofProduct;
        EditText searchtext;
        
        //fragment declaration 
       

        //toolbaar for navigation view 
        Android.Support.V7.Widget.Toolbar toolbar;
        Android.Support.V4.Widget.DrawerLayout drawerLayout;
        Android.Support.Design.Widget.NavigationView navigationView;

        //navigation image view
        ImageView navigationSearch;
  
  


       

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            //navigation item initializations
            toolbar = (Android.Support.V7.Widget.Toolbar)FindViewById(Resource.Id.toolbar);
            drawerLayout = (Android.Support.V4.Widget.DrawerLayout)FindViewById(Resource.Id.drawerLayout);
            navigationView = (Android.Support.Design.Widget.NavigationView)FindViewById(Resource.Id.navview);
            navigationSearch = (ImageView)FindViewById(Resource.Id.navigationSearch);

            searchtext = (EditText)FindViewById(Resource.Id.mainSearch);
            //Recycler View
            productDisplay = (RecyclerView)FindViewById(Resource.Id.mainrecyclerView);



          




            
            //Navigation Event Handler 
            navigationView.NavigationItemSelected += NavigationView_NavigationItemSelected;

            //Setup
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = "LifeLine";
            Android.Support.V7.App.ActionBar actionbar = SupportActionBar;
            actionbar.SetHomeAsUpIndicator(Resource.Drawable.list);
            
            actionbar.SetDisplayHomeAsUpEnabled(true);

            //product click events 
           

            //click events

            
            navigationSearch.Click += NavigationSearch_Click;

            searchtext.TextChanged += Searchtext_TextChanged;

            //retrieve user info on logon 
            UserListener userListener = new UserListener();
            userListener.FetchUser();

            //calling recycler view 
            fetchpost();
          
        }

        private void Searchtext_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
           // List<MainProductDisplay> searchResult =
             //   (from product in MainProductDisplay
               //  where product.Productname.ToLower().Contains(searchtext.Text.ToLower()) select product).Tolist();
        }

        private void NavigationSearch_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(SearchActivity));
            StartActivity(intent);
        }

        private void NavigationFilter_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(SpinnerTest));
            StartActivity(intent);
        }

        private void NavigationView_NavigationItemSelected(object sender, Android.Support.Design.Widget.NavigationView.NavigationItemSelectedEventArgs e)
        {
            if (e.MenuItem.ItemId == Resource.Id.homeNavMenu)
            {
                Intent intent = new Intent(this, typeof(MainActivity));
                StartActivity(intent);
            }
            else if (e.MenuItem.ItemId == Resource.Id.orderHomeNavMenu)
            {
                //Call order home activity
                
            }else if(e.MenuItem.ItemId == Resource.Id.categoriesNavMenu)
            {
                Intent intent = new Intent(this, typeof(Category));
                StartActivity(intent);
            }
            else if (e.MenuItem.ItemId == Resource.Id.cartNavMenu)
            {
                //call cart nav menu activity 
            }
            else if (e.MenuItem.ItemId == Resource.Id.myAccountNavMenu)
            {

                Intent intent = new Intent(this, typeof(MyAccount));
                StartActivity(intent);
            }
            else if (e.MenuItem.ItemId == Resource.Id.callusNavMenu)
            {
                var phone = "+91 8800235803";
                Android.Support.V7.App.AlertDialog.Builder CallAlert = new Android.Support.V7.App.AlertDialog.Builder(this);
                CallAlert.SetMessage("Call Help And Support");

                CallAlert.SetPositiveButton("Call", (alert, args) =>
                 {
                     var uri = Android.Net.Uri.Parse("tel:" + phone);
                     var intent = new Intent(Intent.ActionDial, uri);
                     StartActivity(intent);
                 });
                CallAlert.SetNegativeButton("Cancel", (alert, args) =>
                {
                    CallAlert.Dispose();
                });

                CallAlert.Show();
                
            }
            else if (e.MenuItem.ItemId == Resource.Id.popularQuestionNavMenu)
            {


                Intent intent = new Intent(this, typeof(popularQuestionWebView));
                StartActivity(intent);



            }
            else if (e.MenuItem.ItemId == Resource.Id.helpandFeedbackNavMenu)
            {
                Intent intent = new Intent(this, typeof(HelpAndfeedBack));
                StartActivity(intent);

            }
            else if (e.MenuItem.ItemId == Resource.Id.workininLifeLineNavMenu)
            {
                Intent intent = new Intent(this, typeof(WorkingInLifeLIne));
                StartActivity(intent);

            }
            else if (e.MenuItem.ItemId == Resource.Id.termsOfUseNavMenu)
            {
                Intent intent = new Intent(this, typeof(TermsOfUse));
                StartActivity(intent);

            }
            else if (e.MenuItem.ItemId == Resource.Id.contactsNavMenu)
            {
                Intent intent = new Intent(this, typeof(Contacts));
                StartActivity(intent);


            }
            else if (e.MenuItem.ItemId == Resource.Id.faqNavMenu)
            {
                AppDataHelper.GetFirebaseAuth().SignOut();
                StartActivity(typeof(SplashScreen));
                Finish();
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer((int)GravityFlags.Left);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
            
        }

        void fetchpost()
        {
            product product = new product();
            product.FetchPost();
            product.OnProductRestrieved += Product_OnProductRestrieved;

        }

        private void Product_OnProductRestrieved(object sender, product.PostEventArgs e)
        {
            ListofProduct = new List<MainProductDisplay>();
            ListofProduct = e.mainProductDisplays;

            
            
            
            
            SetupRecyclerView();


        }

        void CreateData()
        {
            ListofProduct = new List<MainProductDisplay>();
            ListofProduct.Add(new MainProductDisplay { ProductName = "Apple Mac Book Pro 1 TB" });
            ListofProduct.Add(new MainProductDisplay { ProductName = "Amd Ryzen Threadripper" });
            ListofProduct.Add(new MainProductDisplay { ProductName = "Apple Iphone 11 Pro Max" });
        }

        void SetupRecyclerView()
        {
            productDisplay.SetLayoutManager(new Android.Support.V7.Widget.LinearLayoutManager(productDisplay.Context));
            mainProductDisplayAdapter = new mainProductDisplayAdapter(ListofProduct);
            productDisplay.SetAdapter(mainProductDisplayAdapter);
        }
        

    }
}