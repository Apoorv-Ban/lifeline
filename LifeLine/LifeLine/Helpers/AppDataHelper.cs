﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase;
using Firebase.Auth;
using Firebase.Firestore;

namespace LifeLine.Helpers
{
   public static class AppDataHelper
    {
        static ISharedPreferences preferences = Application.Context.GetSharedPreferences("userinfo", FileCreationMode.Private);
        static ISharedPreferencesEditor editor;
        public static FirebaseFirestore GetFirestore()
        {
            var app = FirebaseApp.InitializeApp(Application.Context);
            FirebaseFirestore database;

            if (app == null)
            {
                var options = new FirebaseOptions.Builder()
                    .SetProjectId("lifeline-f3274")
                    .SetApplicationId("lifeline-f3274")
                    .SetApiKey("AIzaSyBd2cVH4-HNrE5QVAKk7Cd6SYrz4zhh52U")
                    .SetDatabaseUrl("https://lifeline-f3274.firebaseio.com")
                    .SetStorageBucket("lifeline-f3274.appspot.com")
                    .Build();

                app = FirebaseApp.InitializeApp(Application.Context, options);
                database = FirebaseFirestore.GetInstance(app);



            }
            else
            {
                database = FirebaseFirestore.GetInstance(app);
            }
            return database;
        }

        public static FirebaseAuth GetFirebaseAuth()
        {
            var app = FirebaseApp.InitializeApp(Application.Context);
            FirebaseAuth mAuth;

            if (app == null)
            {
                var options = new FirebaseOptions.Builder()
                    .SetProjectId("lifeline-f3274")
                    .SetApplicationId("lifeline-f3274")
                    .SetApiKey("AIzaSyBd2cVH4-HNrE5QVAKk7Cd6SYrz4zhh52U")
                    .SetDatabaseUrl("https://lifeline-f3274.firebaseio.com")
                    .SetStorageBucket("lifeline-f3274.appspot.com")
                    .Build();

                app = FirebaseApp.InitializeApp(Application.Context, options);
                mAuth = FirebaseAuth.Instance;




            }
            else
            {
                mAuth = FirebaseAuth.Instance;
            }
            return mAuth;
        }

        public static void Saveinfo(string name , string phonenumber)
        {
            editor = preferences.Edit();
            editor.PutString("name", name);
            editor.PutString("phonenumber", phonenumber);
            
            editor.Apply();
        }

        public static void Saveinfo( string name)
        {
            editor = preferences.Edit();
            
            editor.PutString("name",name);
            editor.Apply();
        }

        public static string Getinfo()
        {
            string name = "";
            name = preferences.GetString("name", "");
            return name;

        }


    }
}