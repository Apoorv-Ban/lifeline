﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Tasks;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Firestore;
using LifeLine.DataModels;
using LifeLine.Helpers;

namespace LifeLine.EventListeners
{
    class product : Java.Lang.Object , IOnSuccessListener
    {

        
        public List<MainProductDisplay> ListofProducts = new List<MainProductDisplay>();

        public event EventHandler<PostEventArgs> OnProductRestrieved;

        public class PostEventArgs : EventArgs 
        {
            public List<MainProductDisplay> mainProductDisplays { get; set; }
        }

        public void FetchPost()
        {
            AppDataHelper.GetFirestore().Collection("products").Get()
                .AddOnSuccessListener(this);
        }

       

           public void RemoveListener()
        {
           

        }

        public void OnSuccess(Java.Lang.Object result)
        {
            var snapshot = (QuerySnapshot)result;

            if (!snapshot.IsEmpty)
            {
                foreach(DocumentSnapshot item in snapshot.Documents)
                {
                    MainProductDisplay mainProductDisplay = new MainProductDisplay();

                    mainProductDisplay.ProductName = item.Get("name") != null ? item.Get("name").ToString(): "";
                    mainProductDisplay.Price = item.Get("price") != null ? item.Get("price").ToString() : "";
                    mainProductDisplay.ImageUrl = item.Get("image_url") != null ? item.Get("image_url").ToString() : "";
                        
                    ListofProducts.Add(mainProductDisplay);


                 
                }

                OnProductRestrieved?.Invoke(this, new PostEventArgs { mainProductDisplays = ListofProducts });
            }

        }
    }
}