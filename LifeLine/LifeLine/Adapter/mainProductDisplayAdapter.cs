﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using LifeLine.DataModels;
using FFImageLoading;
using static Android.Widget.AdapterView;
using Android.Content;
using LifeLine.Resources.Activites;

namespace LifeLine.Adapter
{
    class mainProductDisplayAdapter : RecyclerView.Adapter  , ItemClickListener
    {
        //click listeners 
      

        public event EventHandler<mainProductDisplayAdapterClickEventArgs> ItemClick;
        public event EventHandler<mainProductDisplayAdapterClickEventArgs> ItemLongClick;
        List <MainProductDisplay> items;
       
        public mainProductDisplayAdapter(List<MainProductDisplay> data)
        {
            items = data; 
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = null;


            itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.productDisplay, parent, false);

            
            

            var vh = new mainProductDisplayAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

      



        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = items[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as mainProductDisplayAdapterViewHolder;
            //holder.TextView.Text = items[position];

            holder.productName.Text = item.ProductName;
            holder.productPrice.Text = item.Price;
            Getimage(item.ImageUrl, holder.productImageView);
        }

        void Getimage(string url, ImageView imageView)
        {
            ImageService.Instance.LoadUrl(url)
                .Retry(3, 200)
                .DownSample(400, 400)
                .Into(imageView);
        }

        public override int ItemCount => items.Count;

        void OnClick(mainProductDisplayAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(mainProductDisplayAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

        public void OnClick(View itemView, int position, bool isLongClick)
        {
            if (isLongClick)
                Console.WriteLine("clicked");
            else
                Console.WriteLine("clicked");
                
            


        }
    }

    public class mainProductDisplayAdapterViewHolder : RecyclerView.ViewHolder , View.IOnClickListener, View.IOnLongClickListener
    {
        //public TextView TextView { get; set; }

        private ItemClickListener itemClickListener;
        public TextView productName;
        public TextView productPrice; 
        public ImageView productImageView;
        public ImageView addtoCartImageView;

        public mainProductDisplayAdapterViewHolder(View itemView, Action<mainProductDisplayAdapterClickEventArgs> clickListener,
                            Action<mainProductDisplayAdapterClickEventArgs> longClickListener) : base(itemView)
        {

            productName = (TextView)itemView.FindViewById(Resource.Id.mainPageProductName);
            productPrice = (TextView)itemView.FindViewById(Resource.Id.mainPageProductPrice);
            productImageView = (ImageView)itemView.FindViewById(Resource.Id.mainPageProductImage);
            addtoCartImageView = (ImageView)itemView.FindViewById(Resource.Id.mainPageProductCart);


            itemView.SetOnClickListener(this);
            itemView.SetOnLongClickListener(this);





            //TextView = v;
            itemView.Click += (sender, e) => clickListener(new mainProductDisplayAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new mainProductDisplayAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }

        public void SetItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }



        public void OnClick(View v)
        {
            itemClickListener.OnClick(v, AdapterPosition, false);
        }

        public bool OnLongClick(View v)
        {
            itemClickListener.OnClick(v, AdapterPosition, true);
            return true;
        }

    }

    public class mainProductDisplayAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}