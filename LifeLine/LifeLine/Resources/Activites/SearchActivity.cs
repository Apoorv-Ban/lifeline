﻿using Android.App;
using Android.Gms.Tasks;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Widget;
using LifeLine.Adapter;
using LifeLine.DataModels;
using LifeLine.Helpers;
using System;
using System.Collections.Generic;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = true)]
    public class SearchActivity : AppCompatActivity, IOnSuccessListener
    {



        //defining variables
        EditText search;
        ImageView searchicon;
        TextView searchResultTextView;
        //adapter declaration 
        RecyclerView searchProductDisplay;
        mainProductDisplayAdapter mainProductDisplayAdapter;
        SearchAdapter searchAdapter;
        List<MainProductDisplay> ListofProduct;
        List<SearchProductDisplay> SearchProduct;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.SearchResult);

            //initializing views
            search = (EditText)FindViewById(Resource.Id.searchEditText);
            searchicon = (ImageView)FindViewById(Resource.Id.searchSearch);
            searchResultTextView = (TextView)FindViewById(Resource.Id.searchresultTextview);

            //initializing recycler views 
            searchProductDisplay = (RecyclerView)FindViewById(Resource.Id.searchrecyclerview);



            //click events 
            searchicon.Click += Searchicon_Click;


            //calling fetchpost

            }



        private void Searchicon_Click(object sender, EventArgs e)
        {
            if (searchResultTextView.Visibility == Android.Views.ViewStates.Gone && searchProductDisplay.Visibility == Android.Views.ViewStates.Gone)
            {
                string searchResult = search.Text;
                Console.WriteLine(searchResult);
                fetchpost(searchResult);
                searchProductDisplay.Visibility = Android.Views.ViewStates.Visible;
                searchResultTextView.Visibility = Android.Views.ViewStates.Visible;
            }
            else
            {
                searchProductDisplay.ClearFocus();
                searchProductDisplay.Visibility = Android.Views.ViewStates.Gone;
                searchResultTextView.ClearFocus();
                searchResultTextView.Visibility = Android.Views.ViewStates.Gone;
            }
        }


        void SetupRecyclerView()
        {
            searchProductDisplay.SetLayoutManager(new Android.Support.V7.Widget.LinearLayoutManager(searchProductDisplay.Context));

            searchAdapter = new SearchAdapter(SearchProduct);
            searchProductDisplay.SetAdapter(searchAdapter);
        }

        void fetchpost(string searchResult)
        {
            AppDataHelper.GetFirestore().Collection("products").WhereEqualTo("name".ToLower(), searchResult.ToLower()).Get().AddOnSuccessListener(this);
            SetupRecyclerView();

        }

        public void OnSuccess(Java.Lang.Object result)
        {

            SearchProduct = new List<SearchProductDisplay>();


            SetupRecyclerView();
        }



    }
}

