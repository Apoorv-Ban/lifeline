﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Auth.Api;
using Android.Gms.Auth.Api.SignIn;
using Android.Gms.Common.Apis;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Firebase.Auth;
using Firebase.Firestore;
using Java.Util;
using LifeLine.EventListeners;
using LifeLine.Fragments;
using LifeLine.Helpers;
using Xamarin.Essentials;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = false)]
    public class Signin : AppCompatActivity
    {
        //adding listeners
        TaskCompletionListeners taskCompletionListeners = new TaskCompletionListeners();

        //declaring firebase auth
        FirebaseAuth mAuth;
        //Text View Declaration
       public EditText emailEditText, passwordEditText;

        //Button
        Button signinButton;

        //imageButton
        ImageButton gSignIn;

        //firebase database
         FirebaseFirestore database;

        //TextView
        TextView dontHaveAnAccountTextView;

        //dialogue  fragment
        ProgressDialogueFragment progressDialogue;

        //google signin
        ImageButton sgoogle;
        GoogleSignInOptions gso;
        GoogleApiClient googleApiClient;


        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Signin);
            //temp variables 



            //Calling views 

            sgoogle = (ImageButton)FindViewById(Resource.Id.sgoogle);
            passwordEditText = (EditText)FindViewById(Resource.Id.passwordEdittext);
            emailEditText = (EditText)FindViewById(Resource.Id.emailEditText);
            signinButton = (Button)FindViewById(Resource.Id.signinbutton);
            gSignIn = (ImageButton)FindViewById(Resource.Id.sgoogle);
            dontHaveAnAccountTextView = (TextView)FindViewById(Resource.Id.dont);

            //google sign in setup 
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
                .RequestIdToken("15159003538-cdn86hne3jm7pba1s8g285ff3kubsct3.apps.googleusercontent.com")
                .RequestEmail()
                .Build();

            googleApiClient = new GoogleApiClient.Builder(this)
                .AddApi(Auth.GOOGLE_SIGN_IN_API, gso).Build();
            googleApiClient.Connect();

            //click Events
            sgoogle.Click += Sgoogle_Click;
            signinButton.Click += SigninButton_Click;
            dontHaveAnAccountTextView.Click += DontHaveAnAccountTextView_Click;

            mAuth = AppDataHelper.GetFirebaseAuth();
            database = AppDataHelper.GetFirestore();
        }

        private void Sgoogle_Click(object sender, EventArgs e)
        {
            var intent = Auth.GoogleSignInApi.GetSignInIntent(googleApiClient);
            StartActivityForResult(intent, 3);
        }

        private void DontHaveAnAccountTextView_Click(object sender, EventArgs e)
        {        
            Intent intent = new Intent(this, typeof(Signup));
            StartActivity(intent);
        }

        private void SigninButton_Click(object sender, EventArgs e)//Temp Login Credentials
        {
            //temp variables
            string email = emailEditText.Text, password = passwordEditText.Text;

            if (!email.Contains("@"))
            {
                Toast.MakeText(this, "Please enter the valid email address", ToastLength.Short).Show();
            }
            else if (password.Length < 4) {

                Toast.MakeText(this, "Please enter the valid password", ToastLength.Short).Show();
            } else if (email == null && password == null)
            {
                Toast.MakeText(this, "Please enter the credentials ", ToastLength.Short).Show();
            }
            ShowProgressDialogue("Verifying You......");
            mAuth.SignInWithEmailAndPassword(email, password).AddOnSuccessListener(taskCompletionListeners)
                .AddOnFailureListener(taskCompletionListeners);

            taskCompletionListeners.Sucess += (success, args) =>
            {
                CloseProgressDialogue();
                Toast.MakeText(this, "Login Succesfull", ToastLength.Short).Show();
                
                Intent intent = new Intent(this, typeof(MainActivity));
                StartActivity(intent);
            };

            taskCompletionListeners.Failure += (failure, args) =>
            {
                CloseProgressDialogue();
                Toast.MakeText(this, "Login Failed :" + args.Cause, ToastLength.Short).Show();
            };
         
        
        }
        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if(requestCode == 3)
            {
                GoogleSignInResult result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);
                if (result.IsSuccess)
                {
                    GoogleSignInAccount account = result.SignInAccount;
                    LoginwithFirebase(account);
                }
            }
        }

        private void LoginwithFirebase(GoogleSignInAccount account)
        {
            var credentials = GoogleAuthProvider.GetCredential(account.IdToken, null);
            mAuth.SignInWithCredential(credentials).AddOnSuccessListener(this,taskCompletionListeners)
                .AddOnFailureListener(this,taskCompletionListeners);

            taskCompletionListeners.Sucess += (success, args) =>
            {
                string email = mAuth.CurrentUser.Email;
                string phonenumber = mAuth.CurrentUser.PhoneNumber;
                string name = mAuth.CurrentUser.DisplayName;

                HashMap userMap = new HashMap();
                userMap.Put("email", email);
                userMap.Put("phonenumber", phonenumber);
                userMap.Put("name", name);

                DocumentReference userReference = database.Collection("users").Document(mAuth.CurrentUser.Uid);
                userReference.Set(userMap);

                CloseProgressDialogue();
                Intent intent = new Intent(this, typeof(MainActivity));
                StartActivity(intent);
            };

            taskCompletionListeners.Failure += (failure, args) =>
            {
                Toast.MakeText(this, "Login Failed :" + args.Cause, ToastLength.Short).Show();
            };
        }

        void ShowProgressDialogue(string status)
        {

            progressDialogue = new ProgressDialogueFragment(status);
            var trans = SupportFragmentManager.BeginTransaction();
            progressDialogue.Cancelable = false;
            progressDialogue.Show(trans, "Progress");
        
        }
          void CloseProgressDialogue()
        {
            if(progressDialogue != null)
            {
                progressDialogue.Dismiss();
                progressDialogue = null;
            }
        }
    
    
    }

}