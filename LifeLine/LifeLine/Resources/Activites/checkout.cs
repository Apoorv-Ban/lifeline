﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = false)]
    public class checkout : AppCompatActivity
    {
        //variable declarations 
        ImageView address, payment, summary;
        EditText street1, street2, city, state;
        Button next, back;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Checkout);

            // initialising views
            address = FindViewById<ImageView>(Resource.Id.coutOneAddress);
            payment = FindViewById<ImageView>(Resource.Id.coutOnePayment);
            summary = FindViewById<ImageView>(Resource.Id.coutOneSummary);

            street1 = (EditText)FindViewById(Resource.Id.street1EditText);
            street2 = (EditText)FindViewById(Resource.Id.street2Edittext);
            city = (EditText)FindViewById(Resource.Id.cityEditText);
            state = (EditText)FindViewById(Resource.Id.stateEdittext);

            next = (Button)FindViewById(Resource.Id.nextButton);
            back = (Button)FindViewById(Resource.Id.backButton);

            //click events 
            next.Click += Next_Click;
            back.Click += Back_Click;
        }

        private void Back_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
        }

        private void Next_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(checkouttwo));
            StartActivity(intent);
        }
    }
}