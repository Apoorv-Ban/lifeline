package crc6428d87a240df02b4a;


public class WorkingInLifeLIne
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("LifeLine.Resources.Activites.WorkingInLifeLIne, LifeLine", WorkingInLifeLIne.class, __md_methods);
	}


	public WorkingInLifeLIne ()
	{
		super ();
		if (getClass () == WorkingInLifeLIne.class)
			mono.android.TypeManager.Activate ("LifeLine.Resources.Activites.WorkingInLifeLIne, LifeLine", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
