﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using LifeLine.Resources.Classes;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = false)]
    public class checkouttwo : AppCompatActivity
    {
        // declaring variables
        ImageView address, payment, summary, paytm, gpay, card;
        Button next, back;
        EditText nameoncard, cardnumber, expiry, cvv;


        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Checkoutone);
            address = (ImageView)FindViewById(Resource.Id.couttwoaddress);
            payment = (ImageView)FindViewById(Resource.Id.couttwopayments);
            summary = (ImageView)FindViewById(Resource.Id.couttwosummary);
            paytm = (ImageView)FindViewById(Resource.Id.paymentPaytm);
            gpay = (ImageView)FindViewById(Resource.Id.gpayPayment);
            card = (ImageView)FindViewById(Resource.Id.cardpayment);
            nameoncard = (EditText)FindViewById(Resource.Id.nameoncardEditText);
            cardnumber = (EditText)FindViewById(Resource.Id.cardnumberEdittext);
            expiry = (EditText)FindViewById(Resource.Id.cardexpiraydateEditText);
            cvv = (EditText)FindViewById(Resource.Id.cardcvvEdittext);
            next = (Button)FindViewById(Resource.Id.couttwonextButton);
            back = (Button)FindViewById(Resource.Id.couttwobackButton);


            next.Click += Next_Click;
            back.Click += Back_Click;
            address.Click += Address_Click;
            payment.Click += Payment_Click;
            summary.Click += Summary_Click;

            cardconnect();
        }

        private void Summary_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
        }

        private void Payment_Click(object sender, EventArgs e)
        {
            Toast.MakeText(this, "Enter your payment details ", ToastLength.Short).Show();
        }

        private void Address_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(checkout));
            StartActivity(intent);
        }

        private void Back_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(checkout));
            StartActivity(intent);
        }

        private void Next_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(CheckoutThree));
            StartActivity(intent);
        }
    
     public void cardconnect()
        {
            UserCardPayment card = new UserCardPayment()
            {
                user_CardNumber = cardnumber.Text ,
                user_CardCVV =   cvv.Text,
                user_CardName = nameoncard.Text,
                user_CardExpiry = expiry.Text,

                                          

            };
        
        }
    
    
    }
}    