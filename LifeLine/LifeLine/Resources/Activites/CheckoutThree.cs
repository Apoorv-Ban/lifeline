﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = false)]
    public class CheckoutThree : AppCompatActivity
    {
        //declaring variables 
        Button next, back;
        ImageView payments, summary, address;
        //TextView shippingAddress ;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.checkouttwo);

            //initialising values 
            next = (Button)FindViewById(Resource.Id.coutthreenextbutton);
            back = (Button)FindViewById(Resource.Id.coutthreebackButton);
            payments = (ImageView)FindViewById(Resource.Id.coutthreePayments);
            address = (ImageView)FindViewById(Resource.Id.coutthreeAddress);
            summary = (ImageView)FindViewById(Resource.Id.coutthreeSummary);

            //click events 
            next.Click += Next_Click;
            back.Click += Back_Click;







        }

        private void Back_Click(object sender, EventArgs e)
        {

            Intent intent = new Intent(this, typeof(checkouttwo));
            StartActivity(intent);
        }

        private void Next_Click(object sender, EventArgs e)
        {
            Toast.MakeText(this, "Order Succesfully You Can track Your order in Track order section ", ToastLength.Short).Show();
            Intent intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
        }
    }
}