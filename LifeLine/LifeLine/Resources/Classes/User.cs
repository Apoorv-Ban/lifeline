﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Org.Apache.Http.Authentication;

namespace LifeLine.Resources.Activites
{
    class User
    {
        // adding attributes to class to connect values around all activities 
        public string user_name{get; set;}
        public string user_password { get; set; } 
        public string user_email { get; set; }
        public string user_phone_number { get; set; }
        public string user_address { get; set; }

        
    }
}