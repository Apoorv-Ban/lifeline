﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace LifeLine.Resources.Classes
{
    class UserCardPayment
    {
        //attributes for saving card payment details 
        public string user_CardNumber { get; set; }
        public string user_CardCVV { get; set; }
        public string user_CardExpiry { get; set; }
        public string user_CardName { get; set; }
    }
}