﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;

using Android.Runtime;
using Android.Widget;
using Android.Views;
using Android.Support.V4.View;
using Android.Support.V4.App;
using Android.Telephony;
using Java.Util;
using LifeLine.Helpers;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = false)]
    public class MyAccount : AppCompatActivity
    {
        //activity decalration
        Android.Support.V7.Widget.Toolbar toolbar;



        //Variable defination
        public TextView myAccountNameTextView, myAccountPasswordTextView; 
        TextView toAllHistoryTextView;

        //imageView
        ImageView myAccountEditNameImage, myAccountpasswordEditImage;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.MyAccount);


            //setting the toolbar

            toolbar = (Android.Support.V7.Widget.Toolbar)FindViewById(Resource.Id.myAccountToolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = "My Account";

            Android.Support.V7.App.ActionBar actionBar = SupportActionBar;
            actionBar.SetHomeAsUpIndicator(Resource.Drawable.arrowLeft);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            toolbar.Click += Toolbar_Click;

            //displaying Values from Class
           

            //initializing views 
            myAccountNameTextView = (TextView)FindViewById(Resource.Id.myAccountNameTextView);
            myAccountPasswordTextView = (TextView)FindViewById(Resource.Id.myAccountpasswordtextview);
            toAllHistoryTextView = (TextView)FindViewById(Resource.Id.toallHistoryTextView);
            myAccountEditNameImage = (ImageView)FindViewById(Resource.Id.myAccountEditNameimage);
            myAccountpasswordEditImage = (ImageView)FindViewById(Resource.Id.myAccountPasswordEditImage);
            
            

           
            //click events 
            myAccountpasswordEditImage.Click += MyAccountpasswordEditImage_Click;
            myAccountEditNameImage.Click += MyAccountEditNameImage_Click;
        }

        private void Toolbar_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
        }

        private void MyAccountEditNameImage_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(ChangeName));
            StartActivity(intent);
            
            
            
        }

        private void MyAccountpasswordEditImage_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(ChangePassword));
            StartActivity(intent);

        }
       
       
    }
}