package crc6470800471ea43d4c8;


public class SearchAdapterViewHolder
	extends android.support.v7.widget.RecyclerView.ViewHolder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("LifeLine.Adapter.SearchAdapterViewHolder, LifeLine", SearchAdapterViewHolder.class, __md_methods);
	}


	public SearchAdapterViewHolder (android.view.View p0)
	{
		super (p0);
		if (getClass () == SearchAdapterViewHolder.class)
			mono.android.TypeManager.Activate ("LifeLine.Adapter.SearchAdapterViewHolder, LifeLine", "Android.Views.View, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
