﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using LifeLine.Resources.Classes;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = false)]
    public class SpinnerTest : AppCompatActivity
    {
        //price seekbar
        SeekBar priceseekbar;
        TextView seekbarPriceTextView;

        //buttons
        Button clearButton, donebutton;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "Main" layout resource
            SetContentView(Resource.Layout.SpinnerLayout);

            //declaring views
            clearButton = (Button)FindViewById(Resource.Id.clearButton);
            donebutton = (Button)FindViewById(Resource.Id.doneButton);
            priceseekbar = (SeekBar)FindViewById(Resource.Id.priceSeekBar);
            seekbarPriceTextView = (TextView)FindViewById(Resource.Id.priceDisplayTextView);

            //seekbar progress


            priceseekbar.ProgressChanged += (s, e) =>
            {
                seekbarPriceTextView.Text = string.Format("₹{0}", e.Progress); //lambda expression
            };

            //button click events 
            clearButton.Click += ClearButton_Click;
            donebutton.Click += Donebutton_Click;




            //category snipper

            Spinner spinner = FindViewById<Spinner>(Resource.Id.TestSpinner);

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.categories_array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            //popularityspinner

            Spinner popularityspinner = FindViewById<Spinner>(Resource.Id.popularitysnippers);

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(popularityspinner_ItemSelected);
            var popularityadapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.popularity_array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            popularityspinner.Adapter = popularityadapter;

            //color
            Spinner colorspinner = FindViewById<Spinner>(Resource.Id.colorsnippers);

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(colorspinner_ItemSelected);
            var coloradapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.color_array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            colorspinner.Adapter = coloradapter;

            //Rating
            Spinner ratingspinner = FindViewById<Spinner>(Resource.Id.ratingsnippers);

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(ratingspinner_ItemSelected);
            var ratingadapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.rating_array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            ratingspinner.Adapter = ratingadapter;

            //Passing Values into class
            
        }
        //button clicks 
        private void Donebutton_Click(object sender, EventArgs e)
        {
            
            
               
            
            Intent intent = new Intent(this, typeof(SearchActivity));
            StartActivity(intent);
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            priceseekbar.Progress = 0;
            
            
        }

        //spinner events 

        private void ratingspinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var ratingspinner = sender as Spinner;
        }

        private void colorspinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var colorspinner = sender as Spinner;
        }

        private void popularityspinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var popularityspinner = sender as Spinner;
        }

        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var spinner = sender as Spinner;
        }



    }
}

