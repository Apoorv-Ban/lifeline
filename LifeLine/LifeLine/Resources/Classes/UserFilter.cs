﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace LifeLine.Resources.Classes
{
    class UserFilter
    {
        //attributes for saving filter settings 
        public string filter_Categories { get; set; }
        public string filter_Popularity { get; set; }
        public string filter_Color { get; set; }
        public string filter_rating { get; set; }
        public string filter_price { get; set; } 
    }
}