﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = false)]
    public class ChangeName : AppCompatActivity
    {
        MyAccount maccount = new MyAccount();
        //Variable Declaration
        EditText changename;
        ImageView add, back;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ChangeName);
            //intializing views
            changename = (EditText)FindViewById(Resource.Id.changenameEditText);
            add = (ImageView)FindViewById(Resource.Id.confirmChangeName);
            back = (ImageView)FindViewById(Resource.Id.declinechangename);

            add.Click += Add_Click;
            back.Click += Back_Click;
        }

        private void Back_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
        }

        private void Add_Click(object sender, EventArgs e)
        {
           

          
           
            
                Toast.MakeText(this, "Update Successful", ToastLength.Short).Show();

                Intent intent = new Intent(this, typeof(MainActivity));
                StartActivity(intent);
         

        }
    }
}