﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = false)]
    public class WorkingInLifeLIne : Activity
    {
        WebView webview;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.TermsOfUSe);
            //initializing web view
            webview = (WebView)FindViewById(Resource.Id.workinginlifelinewebview);
            webview.SetWebViewClient(new ExtendWebViewClient());
            webview.LoadUrl("https://www.google.com/");
            WebSettings webSettings = webview.Settings;
            webSettings.JavaScriptEnabled = true;

        }
    }

}