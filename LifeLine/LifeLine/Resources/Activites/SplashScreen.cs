﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = false)]
    public class SplashScreen : AppCompatActivity
    {
        //declaring variables 
        TextView contextview;
        ImageView splashScreenArrow;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.SplashScreen);

            // initaializing views 
            contextview = (TextView)FindViewById(Resource.Id.contextview);
            splashScreenArrow = (ImageView)FindViewById(Resource.Id.splashScreenArrow);

            //clickevents

            contextview.Click += Contextview_Click;
            splashScreenArrow.Click += SplashScreenArrow_Click;
        }

        private void SplashScreenArrow_Click(object sender, EventArgs e)
        {

            Intent intent = new Intent(this, typeof(Signup));
            OverridePendingTransition(Resource.Drawable.slide_right, Resource.Drawable.slide_left);
            StartActivity(intent);
        }

        private void Contextview_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Signin));
            OverridePendingTransition(Resource.Drawable.slide_right, Resource.Drawable.slide_left);
            StartActivity(intent);
        }
    }
}