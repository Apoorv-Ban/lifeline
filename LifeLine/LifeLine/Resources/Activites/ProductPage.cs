﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using LifeLine.Adapter;
using LifeLine.DataModels;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "ProductPage")]
    public class ProductPage : AppCompatActivity
    {
        //calling adapter 
        mainProductDisplayAdapter mainProductDisplayAdapter;

        // defining variables 
        ImageView back, search, filter, productimage;
        TextView productname, productprice;
        Button addtocart;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here

            SetContentView(Resource.Layout.Productpage);



            //initializing views 
            back = (ImageView)FindViewById(Resource.Id.productpageback);
            search = (ImageView)FindViewById(Resource.Id.productpagesearch);
            filter = (ImageView)FindViewById(Resource.Id.productpagefilter);
            productname = (TextView)FindViewById(Resource.Id.productpageproductname);
            productimage = (ImageView)FindViewById(Resource.Id.productpageproductimage);
            productprice = (TextView)FindViewById(Resource.Id.productpageproductprice);
            addtocart = (Button)FindViewById(Resource.Id.productpageaddtocart);


            //displaying image 
            

        }
    }
}