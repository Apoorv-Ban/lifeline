﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Webkit;
using Android.Widget;

namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainMenu", MainLauncher = false)]
    public class popularQuestionWebView : AppCompatActivity
    {
        WebView webview;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here

            SetContentView(Resource.Layout.PopularQuestions);

            //initializing web view
            webview = (WebView)FindViewById(Resource.Id.popularQuestionWebView);
            webview.SetWebViewClient(new ExtendWebViewClient());
            webview.LoadUrl("https://www.google.com/");
            WebSettings webSettings = webview.Settings;
            webSettings.JavaScriptEnabled = true;
        }
    }

    internal class ExtendWebViewClient : WebViewClient
    {
#pragma warning disable CS0672 // Member overrides obsolete member
        public override bool ShouldOverrideUrlLoading(WebView view, string url)
#pragma warning restore CS0672 // Member overrides obsolete member
        {
            view.LoadUrl(url);
            return true;
        }

    }
}