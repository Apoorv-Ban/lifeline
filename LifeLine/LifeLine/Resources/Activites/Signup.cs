﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Tasks;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Firebase;
using Firebase.Auth;
using Firebase.Firestore;
using Java.Util;
using LifeLine.EventListeners;
using LifeLine.Fragments;
using LifeLine.Helpers;
using Xamarin.Essentials;


namespace LifeLine.Resources.Activites
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = false)]
    public class Signup : AppCompatActivity 
    {
        // Edit Text declaration
        public EditText signUpEmailEditText, signupPasswordEditText, confirmPasswordEditText, mNumberEditText;

        //button declaration
        ImageView backSignUpButton ;
        Button signupbutton;

        //temp variable declaration
        string email, password, confirmpassword, phonenumber;

        //firebase declaration
        FirebaseFirestore database;
        FirebaseAuth mAuth;

        //Task completion listeners 
        TaskCompletionListeners taskCompletionListeners = new TaskCompletionListeners();
        ProgressDialogueFragment progressDialogue;

        protected override void OnCreate(Bundle savedInstanceState)
        {

            MyAccount main = new MyAccount();
            base.OnCreate(savedInstanceState);

            // Create your application here

            SetContentView(Resource.Layout.Signup);

           

            //views Initialization

            signUpEmailEditText = (EditText)FindViewById(Resource.Id.signupEmailEditText);
            signupPasswordEditText = (EditText)FindViewById(Resource.Id.signupPasswordEdittext);
            confirmPasswordEditText = (EditText)FindViewById(Resource.Id.confirmPasswordEditText);
            mNumberEditText = (EditText)FindViewById(Resource.Id.mNumberEdittext);
            backSignUpButton = (ImageView)FindViewById(Resource.Id.backsignup);
            signupbutton = (Button)FindViewById(Resource.Id.signupbutton);

            //Control Variables 
            var email = signUpEmailEditText.Text;
            var password = signupPasswordEditText.Text;
            var confirmpassword = confirmPasswordEditText.Text;
            var mobileNumber = mNumberEditText.Text;
            
            
               // Click Events 

            signupbutton.Click += Signupbutton_Click;
            backSignUpButton.Click += BackSignUpButton_Click;

            database = AppDataHelper.GetFirestore();
            mAuth = AppDataHelper.GetFirebaseAuth();
        }

        private void BackSignUpButton_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Signin));
            StartActivity(intent);
        }

        private void Signupbutton_Click(object sender, EventArgs e)
        {
                             
           
            
            
            //Temporary declaration
          
             email = signUpEmailEditText.Text;
            password = signupPasswordEditText.Text;
             confirmpassword = confirmPasswordEditText.Text;
             phonenumber = mNumberEditText.Text;

            if (!email.Contains("@"))
            {
                
                Toast.MakeText(this, "Please enter a valid email ", ToastLength.Short).Show();
                return;
            }
            else if (password != confirmpassword)
            {
                
                Toast.MakeText(this, "Passwords do not match ", ToastLength.Short).Show();
            }
            else if (phonenumber.Length != 10)
            {
                Toast.MakeText(this, "Enter a valid phone number ", ToastLength.Short).Show();
            }else
            {
                ShowProgressDialogue("Registration in Progress");
                mAuth.CreateUserWithEmailAndPassword(email, password).AddOnSuccessListener(this, taskCompletionListeners)
                    .AddOnFailureListener(this, taskCompletionListeners);

                taskCompletionListeners.Sucess += (success, args) =>
                {
                    HashMap userMap = new HashMap();
                    userMap.Put("email", email);
                    userMap.Put("phonenumber", phonenumber);

                    DocumentReference userReference = database.Collection("users").Document(mAuth.CurrentUser.Uid);
                    userReference.Set(userMap);

                    CloseProgressDialogue();
                    Intent intent = new Intent(this, typeof(MainActivity));
                    StartActivity(intent);
                };

                taskCompletionListeners.Failure += (failure, args) =>
                {
                    Toast.MakeText(this, "Registrattion Failed :" + args.Cause, ToastLength.Short).Show();
                };
            }


        }
        void ShowProgressDialogue(string status)
        {

            progressDialogue = new ProgressDialogueFragment(status);
            var trans = SupportFragmentManager.BeginTransaction();
            progressDialogue.Cancelable = false;
            progressDialogue.Show(trans, "Progress");

        }
        void CloseProgressDialogue()
        {
            if (progressDialogue != null)
            {
                progressDialogue.Dismiss();
                progressDialogue = null;
            }
        }









    }
}